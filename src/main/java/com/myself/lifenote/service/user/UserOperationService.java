package com.myself.lifenote.service.user;

import com.myself.lifenote.service.user.bo.LoginResult;
import com.myself.lifenote.service.user.bo.RegisterResult;
import com.myself.lifenote.controller.user.vo.UserLoginInfo;
import com.myself.lifenote.controller.user.vo.UserRegisterInfo;

/**
 * @author pwm
 * date 2018-11-25 15:57
 */
public interface UserOperationService {

    /**
     * 用户登陆
     *
     * @param userLoginInfo {@link UserLoginInfo} 登陆信息
     * @return {@link LoginResult} 登陆结果
     */
    LoginResult login(UserLoginInfo userLoginInfo);

    /**
     * 用户注册
     *
     * @param userRegisterInfo 用户提供的基本信息
     * @return 返回注册的结果
     */
    RegisterResult register(UserRegisterInfo userRegisterInfo);

}
