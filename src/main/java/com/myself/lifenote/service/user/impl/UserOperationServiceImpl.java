package com.myself.lifenote.service.user.impl;

import com.myself.lifenote.dao.user.UserOperationDao;
import com.myself.lifenote.service.user.bo.LoginResult;
import com.myself.lifenote.service.user.bo.RegisterResult;
import com.myself.lifenote.dao.user.po.User;
import com.myself.lifenote.controller.user.vo.UserLoginInfo;
import com.myself.lifenote.controller.user.vo.UserRegisterInfo;
import com.myself.lifenote.service.user.UserOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author pwm
 * date 2018-11-25 16:00
 */
@Service
public class UserOperationServiceImpl implements UserOperationService {

    private UserOperationDao userOperationDao;

    @Autowired
    public UserOperationServiceImpl(UserOperationDao userOperationDao) {
        this.userOperationDao = userOperationDao;
    }

    @Override
    public LoginResult login(UserLoginInfo userInfo) {
        LoginResult rs = new LoginResult();
        User user = userOperationDao.getUserByUsername(userInfo.getUsername());
        if (user != null) {
            if (user.getPassword().equals(userInfo.getPassword())) {
                rs.setResult(true);
                rs.setLevel(user.getLevel());
            } else {
                rs.setResult(false);
                rs.setMessage("密码不匹配");
            }
        } else {
            rs.setResult(false);
            rs.setMessage("用户名不存在");
        }
        return rs;
    }

    @Override
    public RegisterResult register(UserRegisterInfo userRegisterInfo) {
        RegisterResult rs = new RegisterResult();
        User user = userOperationDao.getUserByUsername(userRegisterInfo.getUsername());
        //用户已经存在
        if (user != null) {
            rs.setResult(false);
            rs.setMessage("已存在重复的用户名");
        } else {
            User newUser = new User();
            newUser.setUsername(userRegisterInfo.getUsername());
            newUser.setPassword(userRegisterInfo.getPassword());
            newUser.setAge(userRegisterInfo.getAge());
            newUser.setEmail(userRegisterInfo.getEmail());
            newUser.setLevel(0);
            newUser.setPhoneNumber(userRegisterInfo.getPhoneNumber());
            if (StringUtils.isEmpty(userRegisterInfo.getNickName())) {
                newUser.setNickName(userRegisterInfo.getUsername());
                rs.setMessage("由于您未填写昵称，使用您的用户名作为昵称");
            }
            userOperationDao.setUser(newUser);
            rs.setResult(true);
        }
        return rs;
    }

}
