package com.myself.lifenote.service.user.bo;

import lombok.Data;

/**
 * 登陆结果
 *
 * @author pwm
 * date 2018-11-25 16:12
 */
@Data
public class LoginResult {

    /**
     * 登陆结果信息
     */
    private boolean result;

    /**
     * 附带信息，如登陆失败的原因
     */
    private String message;

    /**
     * 管理员（2）普通用户（0）
     * 异常时，返回（-999）
     */
    private int level;

}
