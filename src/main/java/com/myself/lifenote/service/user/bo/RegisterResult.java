package com.myself.lifenote.service.user.bo;

import lombok.Data;

/**
 * 注册结果
 *
 * @author pwm
 * date 2018-11-25 19:48
 */
@Data
public class RegisterResult {

    /**
     * 是否注册成功
     */
    private boolean result;

    /**
     * 附带信息，如注册失败的原因
     */
    private String message;

}
