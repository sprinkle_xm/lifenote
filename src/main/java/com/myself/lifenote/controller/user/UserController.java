package com.myself.lifenote.controller.user;

import com.myself.lifenote.service.user.bo.LoginResult;
import com.myself.lifenote.service.user.bo.RegisterResult;
import com.myself.lifenote.controller.user.vo.UserLoginInfo;
import com.myself.lifenote.controller.user.vo.UserRegisterInfo;
import com.myself.lifenote.service.user.UserOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * life note - user
 * 用户的基本操作：注册，登陆，修改个人信息等
 *
 * @author pwm
 * date 2018-11-19 23:26
 */
@RestController
@RequestMapping(value = "/homePage")
public class UserController {

    private UserOperationService userOperationService;

    @Autowired
    public UserController(UserOperationService userOperationService) {
        this.userOperationService = userOperationService;
    }

    /**
     * @param userInfo 登陆信息
     * @return 登陆结果信息
     * @see LoginResult
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ResponseEntity login(@Validated UserLoginInfo userInfo) {
        LoginResult loginResult = userOperationService.login(userInfo);
        return ResponseEntity.ok(loginResult);
    }

    /**
     * @param userRegisterInfo 注册信息
     * @return 注册的结果信息
     * @see RegisterResult
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ResponseEntity<RegisterResult> register(@Validated UserRegisterInfo userRegisterInfo) {
        RegisterResult registerResult = userOperationService.register(userRegisterInfo);
        return ResponseEntity.ok(registerResult);
    }

}
