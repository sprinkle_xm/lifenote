package com.myself.lifenote.controller.user.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author pwm
 * date 2018-11-25 19:54
 */
@Data
public class UserRegisterInfo {

    /**
     * 用户名
     */
    @NotEmpty(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotEmpty(message = "用户密码不能为空")
    @Size(min = 5, max = 15, message = "密码在5-15位数之间")
    private String password;

    /**
     * 年龄
     */
    @Range(min = 1, max = 150, message = "年龄范围在1-150之间")
    private Integer age;

    /**
     * 邮箱
     */
    @Email
    private String email;

    /**
     * 手机号码
     */
    @Pattern(regexp = "\\d+", message = "必须是数字组合")
    private String phoneNumber;

    /**
     * 昵称
     */
    @Size(min = 1, max = 20, message = "昵称在1-20位长度之间")
    private String nickName;

}
