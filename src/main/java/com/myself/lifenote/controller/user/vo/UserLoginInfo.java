package com.myself.lifenote.controller.user.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author pwm
 * date 2018-11-19 23:27
 */
@Data
public class UserLoginInfo {

    /**
     * 用户名
     */
    @NotEmpty(message = "用户名不能为空")
    private String username;

    /**
     * 用户密码
     */
    @NotEmpty(message = "用户密码不能为空")
    private String password;

}
