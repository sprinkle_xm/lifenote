package com.myself.lifenote;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author pwm
 */
@SpringBootApplication
@MapperScan(value = "com.myself.lifenote.dao")
public class LifenoteApplication {

    public static void main(String[] args) {
        SpringApplication.run(LifenoteApplication.class, args);
    }

}

