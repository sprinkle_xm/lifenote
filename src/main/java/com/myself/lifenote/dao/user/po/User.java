package com.myself.lifenote.dao.user.po;

import lombok.Data;

/**
 * @author pwm
 * date 2018-11-25 16:29
 */
@Data
public class User {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phoneNumber;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 用户级别（0-普通，2-管理员，可扩展）
     */
    private int level;

}
