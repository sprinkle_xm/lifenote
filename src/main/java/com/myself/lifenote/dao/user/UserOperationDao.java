package com.myself.lifenote.dao.user;

import com.myself.lifenote.dao.user.po.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author pwm
 * date 2018-11-25 16:29
 */
@Repository
public interface UserOperationDao {

    /**
     * 根据用户名查找用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    User getUserByUsername(@Param(value = "username") String username);

    /**
     * 存储填写的注册信息
     *
     * @param user 用户注册信息
     */
    void setUser(User user);

}
